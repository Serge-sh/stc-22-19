class CycleProgram {
	public static void main(String[] args) {
		int a = 10;
		//Цикл while - это цикл ПОКА. Т.е. Пока условие в скобках верно - цикл будет выполняться снова
		//и снова
		// while(a > 0) {
		// 	System.out.println(a);
		// 	a = a - 1;
		// }

		System.out.println("Cycle for working");
		//3 части
		//1 часть - объявление "бегунка"
		//2 часть - условие, по которому будет работать данный цикл
		//3 часть - манипуляции с бегунком. Что с ним сделать, после окончания круга.
		for (int i = 0; i < 10; i = i + 1) {
			System.out.println(a);
			a = a - 1;
		}
	}
}