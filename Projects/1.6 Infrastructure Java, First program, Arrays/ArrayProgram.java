class ArrayProgram {
	public static void main(String[] args) {
		int[] array = new int[10];

		for (int i = 0; i < array.length; i = i + 1) {
			array[i] = i;
			//10 - "10"
			System.out.println("Index " + i + " = " + array[i]);
		}

	}
}